//
//  ShadowView.swift
//  breakpoint
//
//  Created by Jenia on 7/4/18.
//  Copyright © 2018 Yevheniia Krasotina. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    
    override func awakeFromNib() {
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.black.cgColor
        super.awakeFromNib()
    }

    
    
}
