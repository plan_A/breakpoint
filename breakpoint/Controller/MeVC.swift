//
//  MeVC.swift
//  breakpoint
//
//  Created by Jenia on 7/5/18.
//  Copyright © 2018 Yevheniia Krasotina. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController {
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailLbl.text = Auth.auth().currentUser?.email
    }
    
    @IBAction func signOutBtnWasPressed(_ sender: Any) {
        //sign out code
        
        let logoutPopUp = UIAlertController(title: "Logout?", message: "Are you shure you want to logout?", preferredStyle: .actionSheet)
        
        let logoutAction = UIAlertAction(title: "Logout?", style: .destructive) { (buttonTapped) in
            do {
            try Auth.auth().signOut()
            let authVC = self.storyboard?.instantiateViewController(withIdentifier: "AuthVC") as? AuthVC
                self.present(authVC!, animated: true, completion: nil)
            }
            catch {
                print(error)
            }
        }
        
        logoutPopUp.addAction(logoutAction)
        present(logoutPopUp, animated: true, completion: nil)
        
    }
    
}
