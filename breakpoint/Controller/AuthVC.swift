//
//  AuthVC.swift
//  breakpoint
//
//  Created by Jenia on 7/4/18.
//  Copyright © 2018 Yevheniia Krasotina. All rights reserved.
//

import UIKit
import Firebase

class AuthVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            dismiss(animated: true, completion: nil)
        }
    }

  
    @IBAction func signInWithEmailWasPressed(_ sender: Any) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        present(loginVC!, animated: true, completion: nil)
    }
    

    @IBAction func facebookSignInWasPressed(_ sender: Any) {
        
    }
    
    
    @IBAction func googleSignInWasPressed(_ sender: Any) {
        
    }
    
}
